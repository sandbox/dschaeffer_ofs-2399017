(function ($) {
  Drupal.behaviors.DrawBinaryClock = {
    attach:  function(context) {
      // Retrieve and set settings.
      var beginX = Drupal.settings.binaryclock.startX;
      var beginY = Drupal.settings.binaryclock.startY;
      var gapX = Drupal.settings.binaryclock.gapX;
      var gapY = Drupal.settings.binaryclock.gapY;
      var size = Drupal.settings.binaryclock.size;
	  var hours_on_color = '#' + Drupal.settings.binaryclock.hours_on_color;
	  var hours_off_color = '#' + Drupal.settings.binaryclock.hours_off_color;
	  var minutes_on_color = '#' + Drupal.settings.binaryclock.minutes_on_color;
	  var minutes_off_color = '#' + Drupal.settings.binaryclock.minutes_off_color;
	  var seconds_on_color = '#' + Drupal.settings.binaryclock.seconds_on_color;
	  var seconds_off_color = '#' + Drupal.settings.binaryclock.seconds_off_color;

      // Retrieve the canvas and context.
      var theCanvas = $('#testCanvas');
      var context = null;
      if (theCanvas.get(0).getContext) {
        context = theCanvas.get(0).getContext('2d');
      }
      else {
        alert("Browser does not support canvas");
        quit();
      }

      var w_width = theCanvas.width();
      var h_height = theCanvas.height();

      function drawBinaryClockElement(ctxt, bX, bY, radius, color = 'black', flipped = false) {
		var an_90 = (Math.PI / 2);
		var an_270 = (3 * Math.PI / 2);

        ctxt.beginPath();
        ctxt.arc(bX, bY, radius, an_270, an_90, !flipped);
        ctxt.arc(bX, (bY+(radius+(radius/2))), (radius/2), an_270, an_90, flipped);
        ctxt.arc(bX, bY, 2 * radius, an_90, an_270, flipped);
        ctxt.arc(bX, (bY-(radius+(radius/2))), (radius/2), an_270, an_90, flipped);
        ctxt.closePath();
        ctxt.fillStyle = color;
        ctxt.fill();
      }

      function drawBinaryRow(ctxt, bX, bY, gapY, value = 0, on = 'blue', off = 'black', nElements = 4) {
		var flip = false;
		for(var i = 0; i < nElements; i++) {
          var which_color = off;
          var comparator = value;

          var determinor = comparator & (1 << i);
          if (determinor != 0) {
            which_color = on;
          }
          drawBinaryClockElement(context, bX, bY + (i * gapY), size, which_color, flip);
          flip = !flip;
        }
      }

      function drawBinaryClock(bX, bY, gX, gY, c_hours_on, c_hours_off, c_minutes_on, c_minutes_off, c_seconds_on, c_seconds_off, date) {
        drawBinaryRow(context, bX, bY, gY, date.getHours(), c_hours_on, c_hours_off, 7);
        drawBinaryRow(context, bX + gX, bY, gY, date.getMinutes(), c_minutes_on, c_minutes_off, 7);
        drawBinaryRow(context, bX + (2 * gX), bY, gY, date.getSeconds(), c_seconds_on, c_seconds_off, 7);
      }

      function drawBorder(ctxt, upperLeftX, upperLeftY, width, height) {
        ctxt.beginPath();
        ctxt.moveTo(upperLeftX,upperLeftY);
        ctxt.lineTo(upperLeftX,upperLeftY+height);
        ctxt.lineTo(upperLeftX+width,upperLeftY+height);
        ctxt.lineTo(upperLeftX+width,upperLeftY);
        ctxt.lineTo(upperLeftX,upperLeftY);
        ctxt.closePath();
        ctxt.stroke();
      }

      function setTimeTextFields(date = null) {
		if (date) {
          var hours = date.getHours();
          var minutes = date.getMinutes();
          var seconds = date.getSeconds();

          $('#tHours').val(hours);
          $('#tMinutes').val(minutes);
          $('#tSeconds').val(seconds);
        }
      }

      // Set an interval so that the time is updated ever half second or so.
      setInterval(function(){ updateTime(); }, 50);
	  function updateTime ( ) {
		var date = new Date();

		// Set the time in the textfields.
		setTimeTextFields(date);

		// Prepare to draw.
		context.clearRect(0,0,w_width,h_height);
		drawBorder(context, 0, 0, w_width, h_height);

		// Draw the binary clock in the canvas.
		//drawBinaryClock(beginX, beginY, gapX, gapY, 'green', 'blue', 'red', date);
		drawBinaryClock(beginX, beginY, gapX, gapY, hours_on_color, hours_off_color, minutes_on_color, minutes_off_color, seconds_on_color, seconds_off_color, date);
		
	  }
	}
  }
}(jQuery));