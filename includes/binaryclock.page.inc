<?php

function binaryclock_page() {

  // Add javascript so the browser can render the widget.
  drupal_add_js(drupal_get_path('module', 'binaryclock') . '/js/DrawBinaryClock.js', 'file');
  $settings = array(
    'binaryclock' => array(
      'startX' => intval(variable_get('binaryclock_widget_start_x',0)),
      'startY' => intval(variable_get('binaryclock_widget_start_y',0)),
      'gapX' => intval(variable_get('binaryclock_widget_gap_X',0)),
      'gapY' => intval(variable_get('binaryclock_widget_gap_Y',0)),
      'size' => intval(variable_get('binaryclock_widget_size',0)),
      'hours_on_color' => variable_get('binary_widget_hours_on_color',''),
      'hours_off_color' => variable_get('binary_widget_hours_off_color',''),
      'minutes_on_color' => variable_get('binary_widget_minutes_on_color',''),
      'minutes_off_color' => variable_get('binary_widget_minutes_off_color',''),
      'seconds_on_color' => variable_get('binary_widget_seconds_on_color',''),
	  'seconds_off_color' => variable_get('binary_widget_seconds_off_color',''),
    ),
  );
  drupal_add_js($settings, 'setting');


  return theme('binaryclock_widget');
}