<?php

function binaryclock_admin_page() {
  return drupal_get_form('binaryclock_admin_form');
}

function binaryclock_admin_form() {
  $form = array();

  // Add form element for the width of the widget.
  $form['binaryclock_widget_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('Used to determine the width of the widget.'),
    '#default_value' => variable_get('binaryclock_widget_width',''),
  );

  // Add form element for the height of the widget.
  $form['binaryclock_widget_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Used to determine the height of the widget.'),
    '#default_value' => variable_get('binaryclock_widget_height',''),
  );

  // Add form element for the starting X coordinate of the widget.
  $form['binaryclock_widget_start_x'] = array(
    '#type' => 'textfield',
    '#title' => t('Start X'),
    '#description' => t('Starting X coordinate of the binary clock.'),
    '#default_value' => variable_get('binaryclock_widget_start_x',''),
  );

  // Add form element for the starting Y coordinate of the widget.
  $form['binaryclock_widget_start_y'] = array(
    '#type' => 'textfield',
    '#title' => t('Start Y'),
    '#description' => t('Starting Y coordinate of the binary clock.'),
    '#default_value' => variable_get('binaryclock_widget_start_y',''),
  );

  // Add form element for the gap X.
  $form['binaryclock_widget_gap_X'] = array(
    '#type' => 'textfield',
    '#title' => t('Gap X'),
    '#description' => t('The horizontal gap between vertical columns of the binary clock.'),
    '#default_value' => variable_get('binaryclock_widget_gap_X',''),
  );

  // Add form element for the gap Y.
  $form['binaryclock_widget_gap_Y'] = array(
    '#type' => 'textfield',
    '#title' => t('Gap X'),
    '#description' => t('The vertical gap between each binary element in a row.'),
    '#default_value' => variable_get('binaryclock_widget_gap_Y',''),
  );
  // Add form element for the size.
  $form['binaryclock_widget_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size'),
    '#description' => t('The size of each binary element.'),
    '#default_value' => variable_get('binaryclock_widget_size',''),
  );
  
  // Add form element for the hours on and hours off color.
  $form['binary_widget_hours_on_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('Hours On Color'),
    '#description' => t('Color to use when an Hours Binary Element is on.'),
    '#default_value' => variable_get('binary_widget_hours_on_color',''),
  );
  $form['binary_widget_hours_off_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('Hours Off Color'),
    '#description' => t('Color to use when an Hours Binary Element is off.'),
    '#default_value' => variable_get('binary_widget_hours_off_color',''),
  );

  // Add form element for the minutes on and minutes offcolor.
  $form['binary_widget_minutes_on_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('Minutes On Color'),
    '#description' => t('Color to use when an Minutes Binary Element is on.'),
    '#default_value' => variable_get('binary_widget_minutes_on_color',''),
  );
  $form['binary_widget_minutes_off_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('Minutes Off Color'),
    '#description' => t('Color to use when an Minutes Binary Element is off.'),
    '#default_value' => variable_get('binary_widget_minutes_off_color',''),
  );

  // Add form element for the seconds on and seconds offcolor.
  $form['binary_widget_seconds_on_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('Seconds On Color'),
    '#description' => t('Color to use when an Seconds Binary Element is on.'),
    '#default_value' => variable_get('binary_widget_seconds_on_color',''),
  );
  $form['binary_widget_seconds_off_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('Seconds Off Color'),
    '#description' => t('Color to use when an Seconds Binary Element is off.'),
    '#default_value' => variable_get('binary_widget_seconds_off_color',''),
  );

  return system_settings_form($form);
}